from fastapi import FastAPI, Depends
from schema.predict_input import Predict_Input
from utils.format_input import format_input
from utils.predict import predict as model_predict
from utils.format_predictions import format_predictions
from utils.format_outputs import format_output
from utils.load_model import load_model
from utils.settings import Settings
from utils.get_settings import get_settings

app = FastAPI()


@app.get("/")
async def health(
    settings: Settings = Depends(get_settings)
):
    return {
        "success": True,
        "app_name": settings.app_name,
        "model_name_default": settings.model_name_default,
        "model_names_extra": settings.model_names_extra,
        "cached_models": list(settings.cached_models.keys())
    }


@app.post("/")
async def predict(
    request_data: Predict_Input,
    settings: Settings = Depends(get_settings)
):
    try:
        df_input = format_input(request_data=request_data)
        df_predictions = model_predict(df=df_input, settings=settings)
        predictions = format_predictions(df_predictions=df_predictions)
        return format_output(prediction=predictions[0])
    except Exception as e:
        print(e)
        return format_output(error=e)

@app.on_event('startup')
async def load_models(
):
    settings: Settings = get_settings()
    for model_name in [ settings.model_name_default ] + settings.model_names_extra:
        if model_name in settings.cached_models:
            continue
        load_model(
            model_name=model_name,
            settings=settings
        )
    return {
        "success": True,
        "cached_models": settings.cached_models
    }