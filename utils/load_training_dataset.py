import os
from utils.definitions import (
    DIR_DATA,
    PATH_DATASET,
    DATASET_URL
)
import pandas as pd

def load_training_dataset(
    should_force_reload=False,
    verbose=False
):
    if not should_force_reload and os.path.isfile(PATH_DATASET):
        if verbose:
            print("Loading training dataset from local")
        return pd.read_csv(PATH_DATASET)
    if verbose:
        print("Loading training dataset from s3")
    df = pd.read_csv(DATASET_URL)
    df.to_csv(PATH_DATASET, index=False)
    return df

