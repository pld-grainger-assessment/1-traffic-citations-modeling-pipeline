from functools import partial
def try_to_convert(val, method, default=None):
    try:
        return method(val)
    except:
        return default

try_to_convert_to_int = partial(try_to_convert, method=int)