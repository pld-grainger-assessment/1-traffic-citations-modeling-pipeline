import os
DATASET_URL = 'https://s3-us-west-2.amazonaws.com/pcadsassessment/parking_citations.corrupted.csv'
DIR_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DIR_DATA = os.path.join(DIR_ROOT, 'data')
DIR_MODELS = os.path.join(DIR_ROOT, 'models')
PATH_TOP_25_MAKES = os.path.join(DIR_DATA, 'top_25_makes.json')
PATH_DATASET = os.path.join(DIR_DATA, 'parking_citations.corrupted.csv')
COL_Y = 'score'
COL_Y_HAT = 'prediction'