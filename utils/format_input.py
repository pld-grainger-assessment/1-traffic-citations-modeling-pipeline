from schema.predict_input import Predict_Input
import pandas as pd

def format_input(
    request_data: Predict_Input
) -> pd.DataFrame:
    """
    Convert the input request data into the prediction object expected by
    the model pipeline
    """
    input_data = {
        k.replace("_", " "): v
        for k, v in request_data.dict().items()
    }
    return pd.DataFrame(data=[
        input_data
    ])