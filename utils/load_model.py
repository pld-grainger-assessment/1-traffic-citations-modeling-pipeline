import os
import joblib
from utils.settings import Settings
from utils.definitions import DIR_MODELS

def load_model(
    model_name,
    settings: Settings
):
    '''
    Return the requested model either from the already cached models
    or by loading in the joblib object and storing it into the model cache
    '''
    if model_name in settings.cached_models:
        return settings.cached_models[model_name]

    settings.cached_models[model_name] = joblib.load(
        os.path.join(DIR_MODELS, f"{model_name}.joblib")
    )
    return settings.cached_models[model_name]