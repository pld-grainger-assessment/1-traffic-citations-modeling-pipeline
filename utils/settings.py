from pydantic import BaseSettings
from typing import Dict, List, Any

class Settings(BaseSettings):
    app_name: str = "Make Prediction API"
    model_name_default: str = "v01"
    model_names_extra: List[str] = []
    cached_models: Dict[str, Any] = {}
