def format_output(
    prediction=None,
    error=None
):
    return {
        "success": error is None,
        **({"prediction": prediction} if error is None else {}),
        **({"error": str(error)} if error is not None else {}),

    }