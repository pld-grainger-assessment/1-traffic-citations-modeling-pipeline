from utils.load_model import load_model
from utils.settings import Settings

def predict(
    df,
    settings: Settings,
    model_name=None
):
    '''
    Make predictions on the provided dataset
    either with the model provided or with the default model
    '''
    model_name = model_name if model_name is not None else settings.model_name_default
    model = load_model(model_name=model_name, settings=settings)
    return model.transform(df)
