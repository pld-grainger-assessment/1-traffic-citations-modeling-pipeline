from sklearn.pipeline import Pipeline
# from sklearn.preprocessing import PCA
from pipeline_steps import (
    Label_Encode_Columns,
    Standardize_Time_Column,
    Standardize_Plate_Expiry_Date,
    Standardize_Issue_Date,
    One_Hot_Encode_Columns,
    Drop_Columns
)

def get_feature_engineering_pipeline_v01():
    return Pipeline(
        steps=[
            (
                'Drop Un-used Columns',
                Drop_Columns(
                    columns=[
                        'Meter Id',
                        'Ticket number',
                        'Location',
                        'Violation Description',
                        'Latitude',
                        'Longitude',
                        'Make',
                    ]
                )
            ),
            (
                'Parse Time Column - Issue Time',
                Standardize_Time_Column(col_time='Issue time')
            ),
            (
                'Parse Time Column - Marked Time',
                Standardize_Time_Column(col_time='Marked Time')
            ),
            (
                'Standardize Date - Plate Expiry Date',
                Standardize_Plate_Expiry_Date()
            ),
            (
                'Standardize Date - Issue Date',
                Standardize_Issue_Date()
            ),
            (
                'Label Encode Categorical Columns of Interest',
                Label_Encode_Columns(label_encoding_info={
                    # 'Violation code': None,
                    # 'Route': None,

                    # 'Body Style': None,
                    # 'Color': None,
                    # 'Agency': None,
                    # 'RP State Plate': None,
                }),
            ),
            (
                'One Hot Encode Categorical Columns of Interest',
                One_Hot_Encode_Columns(one_hot_encoding_info={
                    'Violation code': None,
                    'Route': None,
                    'Body Style': None,
                    # 'Color': None,
                    'Agency': None,
                    'RP State Plate': None,
                }),
            ),
            (
                'Drop No Longer Needed Columns',
                Drop_Columns(
                    columns=[
                        'Plate Expiry Date',
                        'Issue time',
                        'Marked Time',
                        'Issue Date',
                        'VIN',
                        'Body Style',
                        'Color',
                        'Agency',
                        'RP State Plate',
                    ]
                )
            )
        ]
    )