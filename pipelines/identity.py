from sklearn.pipeline import Pipeline

from pipeline_steps import (
    Identity
)

def get_identity_pipeline():
    return Pipeline(
        steps=[
            (
                'Identity',
                Identity()
            ),
        ]
    )