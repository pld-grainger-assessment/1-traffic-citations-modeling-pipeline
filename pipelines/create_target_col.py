from sklearn.pipeline import Pipeline
from pipeline_steps import (
    Create_Col_Is_Top_25_Make
)

def get_pipeline_target_col():
    return Pipeline(
        steps=[
            (
                'Create Target Column',
                Create_Col_Is_Top_25_Make()
            )
        ]
    )