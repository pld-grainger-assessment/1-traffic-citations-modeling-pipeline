#%% md

# Training

#%%

import os
import joblib
from utils.load_training_dataset import load_training_dataset
from utils.definitions import (
    DIR_MODELS
)
from pipelines import (
    get_pipeline_target_col,
    get_feature_engineering_pipeline_v01 as get_feature_engineering_pipeline
)
from pipeline_steps import (
    Model_Train_Predict
)
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.pipeline import Pipeline
import lightgbm as lgb
import numpy as np
import pandas as pd
#%%
random_state = 47
np.random.seed(random_state)

#%% md

## Load Data

#%%

df = load_training_dataset()
pipeline_target_col = get_pipeline_target_col()

#%%

y = pipeline_target_col.fit_transform(df)

#%%

df.dtypes


#%% md

## Train Test Split

#%%

X_train, X_test, y_train, y_test = train_test_split(
    df,
    y,
    test_size=0.33,
    random_state=random_state
)
del X_test
del y_test
#%% md

# Balance Dataset

#%%

y_train_false = y_train[y_train == False]
n_true_desired = 2*len(y_train_false)
y_train_true = y_train[y_train == True]
y_train_true = y_train_true.sample(n=n_true_desired) if n_true_desired < len(y_train_true) else y_train_true

y_train = pd.concat([y_train_false, y_train_true])
X_train = X_train.loc[y_train.index]

#%%

X_train.shape

#%% md

# Setup Feature Engineering Pipeline

#%%

pipeline = get_feature_engineering_pipeline()

#%% md

# Setup Train

#%%

model_version = 'v01'

#%%

search_grid_params = {
    'n_estimators': [500],#10, 100,
    'reg_alpha': [0],# , 0.1, 0.5
    'reg_lambda': [0],# , 0.1, 0.5
}

#%%

estimator = lgb.LGBMClassifier(
    importance_type='gain',
    boosting_type='gbdt'
)

#%%

pipeline_predict = Pipeline(steps=pipeline.steps+[
    (
        'Predict',
        Model_Train_Predict(
            estimator=estimator,
            model_version=model_version,
            grid_search_params=search_grid_params,
        )
    )
])



#%%

preds_train = pipeline_predict.fit_transform(X_train, y_train)


#%%

joblib.dump(
    pipeline_predict,
    filename=os.path.join(DIR_MODELS, f"{model_version}.joblib")
)