from pydantic import BaseModel
from typing import Optional

class Predict_Input(BaseModel):
    Ticket_number: str
    Issue_Date: str
    Violation_code: str
    Agency: Optional[float]
    Issue_time: Optional[float]
    Meter_Id: Optional[str]
    Marked_Time: Optional[float]
    RP_State_Plate: Optional[str]
    Plate_Expiry_Date: Optional[float]
    VIN: Optional[str]
    Body_Style: Optional[str]
    Color: Optional[str]
    Location: Optional[str]
    Route: Optional[str]
    Violation_Description: Optional[str]
    Fine_amount: Optional[float]
    Latitude: Optional[float]
    Longitude: Optional[float]