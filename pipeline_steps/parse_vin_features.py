from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from utils.definitions import COL_Y_HAT
from utils.vin_validator import VIN


class Parse_VIN_Features(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        col_Y_hat=None,
        col_vin=None
    ):
        self._col_Y_hat = col_Y_hat if col_Y_hat is not None else COL_Y_HAT
        self._col_vin = col_vin if col_vin is None else 'VIN'

    def _parse_vin_features_for_row(self, s_row):
        vin = s_row[self._col_vin]
        vin_validator = VIN(vin)
        return pd.Series({
            'vin_make': vin_validator.manufacturer
        })

    def _parse_vin_features(self, df):
        df_vin_features = df.apply(self._parse_vin_features_for_row, axis=1)
        return df.merge(
            df_vin_features,
            left_index=True,
            right_index=True,
            how='left'
        )

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return self._parse_vin_features(X)
