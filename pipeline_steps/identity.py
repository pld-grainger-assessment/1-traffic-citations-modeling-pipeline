from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from utils.definitions import COL_Y_HAT


class Identity(BaseEstimator, TransformerMixin):
    def __init__(
        self,
    ):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X
