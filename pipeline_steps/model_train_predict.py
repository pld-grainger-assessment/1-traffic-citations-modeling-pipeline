import os
import joblib
from typing import Dict, Optional
from functools import cached_property
import json
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import GridSearchCV
from utils.definitions import DIR_MODELS

class Model_Train_Predict(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        estimator,
        model_version,
        grid_search_params: Dict[str, Optional[Dict]] = None,
        model=None,
    ):
        self._estimator = estimator
        self._model_version = model_version
        self._grid_search_params = grid_search_params if grid_search_params is not None else {}
        self._model = model

    @cached_property
    def _grid(self):
        return GridSearchCV(
            estimator=self._estimator,
            param_grid=self._grid_search_params
        )

    @cached_property
    def _path_model(self):
        return os.path.join(DIR_MODELS, f"model_{self._model_version}.joblib")

    @cached_property
    def _path_prob_index(self):
        return os.path.join(DIR_MODELS, f"prob_index_{self._model_version}.joblib")

    @cached_property
    def _path_columns(self):
        return os.path.join(DIR_MODELS, f"columns_{self._model_version}.joblib")

    def fit(self, X, y=None):
        if getattr(self, '_model', None) is None:
            print(f"Training on model with {X.columns=}")
            self._model = self._grid.fit(
                X=X,
                y=y
            )
        self._columns = X.columns
        print(f"Finished training on model with {self._columns=}")
        joblib.dump(
            self._columns,
            filename=self._path_columns
        )
        joblib.dump(
            1,
            filename=self._path_prob_index
        )
        joblib.dump(
            self._model,
            filename=self._path_model
        )
        return self

    @cached_property
    def model(self):
        if getattr(self, '_model', None) is None:
            self._model = joblib.load(
                filename=self._path_model
            )
        return self._model

    @cached_property
    def prob_index(self):
        if getattr(self, '_prob_index', None) is None:
            try:
                self._prob_index = joblib.load(
                    filename=self._path_prob_index
                )
            except Exception as e:
                self._prob_index = 1
        return self._prob_index

    @cached_property
    def columns(self):
        if getattr(self, '_columns', None) is None:
            self._columns =  joblib.load(
                    filename=self._path_columns
                )
        return self._columns

    def transform(self, X):
        return pd.DataFrame(data=self.model.predict_proba(X[self.columns].fillna(0)))[self.prob_index].values.tolist()
