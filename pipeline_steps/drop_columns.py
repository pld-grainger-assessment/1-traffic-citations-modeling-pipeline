from sklearn.base import BaseEstimator, TransformerMixin
from functools import cached_property
from utils.definitions import PATH_TOP_25_MAKES
import json

class Drop_Columns(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        columns,
    ):
        self._columns = columns

    def _drop_columns(self, df):
        print(f"Drop columns: {self._columns}")
        return df.drop(columns=self._columns, errors='ignore')

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return self._drop_columns(X)
