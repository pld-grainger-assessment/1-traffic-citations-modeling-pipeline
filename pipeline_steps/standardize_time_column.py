from sklearn.base import BaseEstimator, TransformerMixin
from utils.conversions import try_to_convert_to_int
from functools import cached_property

class Standardize_Time_Column(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        col_time,
    ):
        self._col_time = col_time

    @cached_property
    def _col_name_new(self):
        return self._col_time.lower().replace('time', '').strip().replace(' ', '_')

    def _standardize_issue_date_time(self, df):
        df[f'{self._col_name_new}_minute'] = df[self._col_time].fillna(-999).astype(int).astype(str).replace(
            {'-999': None}).str.slice(start=-2).apply(
            try_to_convert_to_int)
        df[f'{self._col_name_new}_hour'] = df[self._col_time].fillna(-999).astype(int).astype(str).replace(
            {'-999': None}).str.slice(stop=-2).apply(
            try_to_convert_to_int)
        return df

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return self._standardize_issue_date_time(X)
