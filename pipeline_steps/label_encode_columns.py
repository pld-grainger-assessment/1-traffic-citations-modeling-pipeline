from sklearn.base import BaseEstimator, TransformerMixin
from typing import Dict, Optional
from sklearn.preprocessing import LabelEncoder
import pandas as pd

class Label_Encode_Columns(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        label_encoding_info: Dict[str, Optional[Dict]] = None
    ):
        self._label_encoding_info = label_encoding_info if label_encoding_info is not None else {}
        self._label_encoders = {
            col: LabelEncoder()
            for col in self._label_encoding_info.keys()
        }

    def dummy_val_for_col(self, s_col):
        return 'MISSING_VALUE' if s_col.dtype == 'O' else -9999


    def _label_encode_columns(self, X):
        for col, encoder in self._label_encoders.items():
            DUMMY_VAL = self._dummy_vals_per_col[col]
            X.loc[~X[col].isin(encoder.classes_), col] = DUMMY_VAL
            X.loc[:, col] = encoder.transform(X[col])
        return X

    def fit(self, X, y=None):
        self._dummy_vals_per_col = {}
        for col, encoder in self._label_encoders.items():
            self._dummy_vals_per_col[col] = self.dummy_val_for_col(X[col])
            encoder.fit(pd.concat([X[col], pd.Series([self._dummy_vals_per_col[col]])],  axis=0))
        return self

    def transform(self, X):
        return self._label_encode_columns(X)
