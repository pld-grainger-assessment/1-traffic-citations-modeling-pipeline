from sklearn.base import BaseEstimator, TransformerMixin
from typing import Dict, Optional
from sklearn.preprocessing import OneHotEncoder
import numpy as np
import pandas as pd

class One_Hot_Encode_Columns(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        one_hot_encoding_info: Dict[str, Optional[Dict]] = None
    ):
        self._one_hot_encoding_info = one_hot_encoding_info if one_hot_encoding_info is not None else {}
        self._one_hot_encoders = {
            col: OneHotEncoder(handle_unknown='ignore')
            for col in self._one_hot_encoding_info.keys()
        }

    def dummy_vals_for_col(self, s_col):
        return 'MISSING_VALUE' if s_col.dtype == 'O' else -9999.0

    def _one_hot_encode_columns(self, X):
        for col, encoder in self._one_hot_encoders.items():
            DUMMY_VAL = self._dummy_vals_per_col[col]
            X.loc[~X[col].isin(encoder.categories_[0]), col] = DUMMY_VAL
            X_col_one_hot = encoder.transform(X[[col]])
            print(f"One Hot Encode {col=}")
            df_t = pd.DataFrame(data=X_col_one_hot.toarray(), columns=[f"{col}_{cat}" for cat in encoder.categories_[0]], index=X.index)
            X = X.merge(df_t, left_index=True, right_index=True, how='left')
        return X

    def fit(self, X, y=None):
        self._dummy_vals_per_col = {}
        for col, encoder in self._one_hot_encoders.items():
            self._dummy_vals_per_col[col] = self.dummy_vals_for_col(X[col])
            print(f"Fit One Hot Encode {col=}")
            encoder.fit(pd.concat([X[[col]], pd.DataFrame([[self._dummy_vals_per_col[col]]], columns=[col])],  axis=0))
            encoder.fit(X[[col]])
        return self

    def transform(self, X):
        return self._one_hot_encode_columns(X)
