from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from functools import cached_property

class Standardize_Issue_Date(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        col_date=None,
    ):
        self._col_date = col_date if col_date is not None else 'Issue Date'

    @cached_property
    def _col_name_new(self):
        return self._col_date.lower().replace('date', '').strip().replace(' ', '_')

    def _standardize_issue_date(self, df):
        df[self._col_date] = pd.to_datetime(df[self._col_date])
        df[f'{self._col_name_new}_day_of_month'] = df['Issue Date'].dt.day
        df[f'{self._col_name_new}_day_of_year'] = df['Issue Date'].dt.dayofyear
        df[f'{self._col_name_new}_month'] = df['Issue Date'].dt.month
        df[f'{self._col_name_new}_year'] = df['Issue Date'].dt.year
        return df

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return self._standardize_issue_date(X)
