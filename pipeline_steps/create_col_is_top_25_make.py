from sklearn.base import BaseEstimator, TransformerMixin
from functools import cached_property
from utils.definitions import PATH_TOP_25_MAKES
import json

class Create_Col_Is_Top_25_Make(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        col_is_top_25_make='is_top_25_make',
        col_make='Make'
    ):
        self._col_is_top_25_make = col_is_top_25_make
        self._col_make = col_make

    @cached_property
    def _top_25_makes(self):
        with open(PATH_TOP_25_MAKES) as json_file:
            return json.load(json_file)

    def _get_top_25_makes(self, df):
        return df[self._col_make].isin(self._top_25_makes)

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return self._get_top_25_makes(X)
