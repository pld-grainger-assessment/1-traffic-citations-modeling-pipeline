from sklearn.base import BaseEstimator, TransformerMixin
from utils.conversions import try_to_convert_to_int
from functools import cached_property

class Standardize_Plate_Expiry_Date(BaseEstimator, TransformerMixin):
    def __init__(
        self,
        col_date=None,
    ):
        self._col_date = col_date if col_date is not None else 'Plate Expiry Date'

    def _standardize_plate_expiry_date(self, df):
        df['plate_expiry_year'] = df[self._col_date].astype(str).str.slice(stop=4)
        df['plate_expiry_year'] = df['plate_expiry_year'].apply(try_to_convert_to_int).fillna(-999).astype(int).replace(-999, None)

        # %%
        df['plate_expiry_month'] = df['Plate Expiry Date'].astype(str).str.slice(start=4, stop=6)
        df['plate_expiry_month'] = df['plate_expiry_month'].apply(try_to_convert_to_int).fillna(-999).astype(int).replace(-999, None)
        return df

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return self._standardize_plate_expiry_date(X)
