# Traffic Citations Modeling Pipeline

Someone has accidentally deleted the "Make" column from 1/2 of the database, yielding the "parking_citations.corrupted.csv" file.

A business partner has asked you to use the magic of A.I to determine the probability that a user is driving a popular make using the rest of the features trained on the uncorrupted 1/2 of the data.

You've accepted the task.

Build a model to calculate the probability that a car is made by one of the top 25 manufacturers in this dataset based on the uncorrupted features.
        
Deliver an explanation of the quality of this model and feasibility of this task to your imaginary business partner.  Help your business partner understand the strengths and weaknesses of this model.

## Getting Started
### Training the Model and Starting the Prediction Server
To train and deploy the model server you may:
- Run `run.sh` (Defaults to deploying on port 5000)

### Invoking Prediction Server
The prediction server expects a `POST` request with the same columns as in the initial dataframe (with the possible exception of Make)


Notes:
    - There are a couple more columns which may be excluded as they are dropped, but as best practice all columns should be passed
    - Examples of expected POST data may be found in `./tests/cases/main.py`

ex.
```python
import requests
import json
host = "http://127.0.0.1"
port = 5080
input_data = {"Issue Date":"2015-12-21T00:00:00","Issue time":1251.0,"Meter Id":None,"Marked Time":None,"RP State Plate":"CA","Plate Expiry Date":200304.0,"VIN":None,"Make":None,"Body Style":"PA","Color":"GY","Agency":1.0,"Violation code":"4000A1","Fine amount":50.0}
response = requests.post(f'{host}:{port}', data=json.dumps(input_data))
response_data = response.json()
     
```

## Model Server Deliverable:
        
Deploy a local server (that will only be used locally) to allow a user to submit a corrupted row in some format and receive the probability of a top 25 'Make' in return.
    
The server should: 
            - expect a json of features.  Ex: {'Color':'GR', 'Latitude':63453.0} etc.
            - work with a post request
            - return a json with a "prediction" field and the appropriate model output value

I'll test it by launching it myself and querying it from my own machine, so it only needs to work in that context. 

Don't do anything fancy, just get it working.  Your business partner just wants the predictions.  

### Notebooks
### Expository / Exploratory Analysis

Model explanation / analysis should be delivered in a Jupyter notebook.  

Any data analysis, for example your initial data exploration, or anything else of this manner you'd like to share can be delivered as one or more other Jupyter notebooks.

#### VIN Exploration
Deeper dive into the vin data to see how easily the manufacturer may be parsed


#### Top 25 Make Exploration
Gather the top 25 makes needed to make this a binary classification problem and look into any
defining characteristics

#### Feature Engineering
#### Training
#### Model Evaluation


