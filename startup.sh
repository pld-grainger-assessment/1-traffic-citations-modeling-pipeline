#!/bin/bash

if [[ $SHOULD_TRAIN_MODEL ]]
then
  python train_v01.py
fi

uvicorn main:app --reload --port=5000 --host="0.0.0.0"