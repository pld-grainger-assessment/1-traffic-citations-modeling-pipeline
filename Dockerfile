FROM python:3.9-slim
EXPOSE 5000
ENV APP /app
RUN mkdir -p $APP
WORKDIR $APP

RUN apt-get update && apt-get install -y \
    libgomp1

RUN pip install --upgrade pip

COPY requirements.txt .

RUN pip install -r requirements.txt

ADD . ./

ARG SHOULD_TRAIN_MODEL
ENV SHOULD_TRAIN_MODEL=${SHOULD_TRAIN_MODEL}

CMD ./startup.sh


